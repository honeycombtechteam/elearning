//importing of all component here..
import NavigationBar from './components/NavigationBar';
import Footer from './components/Footer';
import Courses from './components/Courses';
import heroImgSection from './components/HeroImgSection';


function App() {
  return (
    <>
      <NavigationBar />
      Hello World !!! I am App.js body
      <heroImgSection />
      <Courses />
      <Footer />
    </>
  );
}

export default App;
